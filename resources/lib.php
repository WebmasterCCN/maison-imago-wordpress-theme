<?php if (!function_exists('require_once_all_regex')):

/**
 * Require once all files in $dir_path that have a filename matching $regex
 * 
 * @param string $dir_path
 * @param string $regex
 */
function require_once_all_regex($dir_path, $regex = "") {

    if ($regex == "") $regex = "//";

    foreach (scandir($dir_path) as $filename) {
        $path = $dir_path . '/' . $filename;
        if ($filename[0] != '.' && is_file($path) && preg_match("/\.php$/i", $path) == 1 && preg_match($regex, $filename) == 1) {
            require_once $path;
        } else if ($filename[0] != '.' && is_dir($path)) {
            require_once_all_regex($path, $regex);
        }
    }
}
endif;
?>