<header class="header sticky grid-container--fit oswald">
    <nav class="navbar navbar-nav navbar-expand-lg  navbar-toggler uppercase align-vertical" id="navbar" >
      <div class="row align-items-center">
        <div class="col col-12 col-lg-3">
          <a  href="{{ home_url('/') }}" >
            <img src="@php 
                      echo get_theme_mod('ccn_imago_maison_logo');
                      @endphp" alt="" class="maison-logo ">
          </a>
        </div>
        <div class="d-lg-none col col-12">
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapse_target" data-target="#border" aria-controls="navbar" aria-expanded="false">
            <span><i class="fas fa-bars"></i></span>
          </button>
        </div>
          
        
      <div class="col col-lg-6 ">
        <nav class="collapse navbar-collapse left-border nav-menu" id="collapse_target">
          @if (has_nav_menu('primary_navigation'))
                {!! wp_nav_menu($primarymenu) !!}
          @endif
        </nav>
      </div>

      <div class="col col-lg-3 col-sm-6">
        <nav class="social-menu  navbar-expand collapse navbar-collapse social-menu-padding" id="collapse_target">      
            @if (has_nav_menu('social'))
              {!! wp_nav_menu($socialmenu) !!}
            @endif
        </nav>
      </div>
    </div>
  </nav>    
</header>
@include('partials.contact-form')

