<footer class="content-info grid-container--fit font oswald">
<div class="row" style="line-height:140%">
  <div id="img" class="col col-12 col-sm-6 col-lg-2 logo-width-div">
    <img src="@php
    echo get_theme_mod('ccn_imago_site_icon_white');
    @endphp" alt="" class="img-fit-div logo-maison-imago">
  </div>

      <div class="col col-12 col-lg-3 col-sm-6 contact-info vl">
        <h3 class="company-title">
        @php
        echo get_theme_mod('ccn_imago_contact_info_company_title') @endphp
        </h3>
        <p>
        @php
        echo get_theme_mod('ccn_imago_contact_info_address');
        echo "<br>";
        echo get_theme_mod('ccn_imago_contact_info_phone_number');
        echo "<br>";
        @endphp
        </p>
      </div>

  <div class="col col-12 col-sm-6 col-lg-2">
    <div class="vertical-center footer-menu">
      <nav >
        {!! wp_nav_menu($footermenu) !!}          
    </nav>
    <a type="link" href="" class="footer-menu" data-toggle="modal" data-target="#myModal">Contact Us</a>
    </div>
    
  </div>

  <div class="col col-12 col-sm-6 col-lg-2 newsletter newsletter-padding">
    <div class="vertical-center">
      <h3>
        Newsletter
      </h3>
      @php wpforms_display(676, false, false) @endphp
    </div>
  </div>

  <div class="col col-12 col-sm-12 col-lg-3 d-flex">

      <img src="@php
          echo get_theme_mod('ccn_imago_logo_1');
          @endphp" class=" img-fit-div vertical-center logo2">

      <img src="@php
        echo get_theme_mod('ccn_imago_community_logo');
        @endphp" alt="" class="img-fit-div vertical-center logo2">

  </div>
</div>
</footer>
@include('partials.contact-form')