<!-- Modal -->
<div class="modal fade padding-top" id="myModal" role="dialog">
  <div class="modal-dialog">
  
    <!-- Modal content-->
    <div class="modal-content ">
      <div class="modal-header padding-for-contact-form secondary-contrast light-gray-background">
        <h1 >Contact Us</h1>
      </div>
      <div class="modal-body secondary-contrast light-gray-background padding-for-contact-form">
        @php 
         wpforms_display(580);
        @endphp
        <button type="button" class="custom-submit no-border" style="float:right" data-dismiss="modal">Close</button>
      </div>
    </div>
    
  </div>
</div>
