@php
        $menuLocations = get_nav_menu_locations(); // Get our nav locations (set in our theme, usually functions.php)
                                           // This returns an array of menu locations ([LOCATION_NAME] = MENU_ID);

        $menuID = $menuLocations['social'];
        $social = wp_get_nav_menu_items($menuID); 
        echo '<li>';
        foreach ( $social as $navItem ) {

echo '<a href="'.$navItem->url.'" title="'.$navItem->title.'"></a>';

}
echo '</li>'