<?php //Pro Details
//tutorial for creating custom theme content https://www.youtube.com/watch?v=YzNfIM_9TaM&t=148s
function ccn_imago_feature_customizer( $wp_customize ) {
     
        //add_section required for creating new section in appearance menu 
        $wp_customize->add_section( 'ccn_imago_feature_section' , array(
            'title'      => __('Contact Info', 'ccn_imago'),
            'priority'   => 1,
        ) );
        
        //Every new editable parameter in section requires 2 function calls add_setting and add_control

        //setting and control cals for contact company title
        $wp_customize->add_setting(
            'ccn_imago_contact_info_company_title',
            array(
                'capability'     => 'edit_theme_options',
                'sanitize_callback' => 'sanitize_text_field',
            )	
        );

        $wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'ccn_imago_contact_info_company_title', array(
            'label' => __('Company title'), 
            'section' => 'ccn_imago_feature_section',
            'settings' => 'ccn_imago_contact_info_company_title',

        ))
    );
        //setting and control cals for contact email adresss
        $wp_customize->add_setting(
            'ccn_imago_contact_info_email',
            array(
                'capability'     => 'edit_theme_options',
                'sanitize_callback' => 'sanitize_text_field',
            )	
        );

        $wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'ccn_imago_contact_info_email', array(
            'label' => __('Contact email'),
            'type' => 'email',
            'section' => 'ccn_imago_feature_section',
            'settings' => 'ccn_imago_contact_info_email',
            
            ))
        );

        //setting and control cals for contact phone number
        $wp_customize->add_setting(
            'ccn_imago_contact_info_phone_number',
            array(
                'capability'     => 'edit_theme_options',
                'sanitize_callback' => 'sanitize_text_field',
            )	
        );

        $wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'ccn_imago_contact_info_phone_number', array(
            'label' => __('Contact Phone number'), 
            'type' => 'number',
            'section' => 'ccn_imago_feature_section',
            'settings' => 'ccn_imago_contact_info_phone_number',

        ))
    );

            //setting and control cals for contact address
            $wp_customize->add_setting(
                'ccn_imago_contact_info_address',
                array(
                    'capability'     => 'edit_theme_options',
                    'sanitize_callback' => 'sanitize_text_field',
                )	
            );
    
            $wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'ccn_imago_contact_info_address', array(
                'label' => __('Address'), 
                'section' => 'ccn_imago_feature_section',
                'settings' => 'ccn_imago_contact_info_address',
    
            ))
        );

        //setting and control cals for image
        $wp_customize->add_setting(
            'ccn_imago_contact_info_logo',
            array(
                'capability'     => 'edit_theme_options',
                'sanitize_callback' => 'sanitize_text_field',
            )	
        );

        $wp_customize->add_control( new WP_Customize_Cropped_Image_Control( $wp_customize, 'ccn_imago_contact_info_logo', array(
            'label' => __('Logo'), 
            
            'section' => 'ccn_imago_feature_section',
            'settings' => 'ccn_imago_contact_info_logo',
            'width' => 750,
            'height' => 500,

        ))
    );     
    }
    add_action( 'customize_register', 'ccn_imago_feature_customizer' );
?>