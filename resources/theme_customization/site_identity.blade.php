<?php
function ccn_imago_site_identity_feature_customizer( $wp_customize ) {
    /**
* Create Logo Setting and Upload Control
*/

    // add a setting for the site logo
    $wp_customize->add_setting('ccn_imago_site_icon_white', array(
        'capability'     => 'edit_theme_options',
        'sanitize_callback' => 'sanitize_text_field',
        'priority' => 0
    )	);
    
    // Add a control to upload the logo
    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'ccn_imago_site_icon_white',
    array(
    'label' => __('Site Icon White'),
    'section' => 'title_tagline',
    'settings' => 'ccn_imago_site_icon_white',
    ) ) );

    // add a setting for the site community logo
    $wp_customize->add_setting('ccn_imago_community_logo', array(
        'capability'     => 'edit_theme_options',
        'sanitize_callback' => 'sanitize_text_field',

    )	);
    
    // Add a control to upload the community logo
    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'ccn_imago_community_logo',
    array(
    'label' => __('Comunity Logo'),
    'section' => 'title_tagline',
    'settings' => 'ccn_imago_community_logo',
    ) ) );
   

    // add a setting for the site logo 1
    $wp_customize->add_setting('ccn_imago_logo_1', array(
        'capability'     => 'edit_theme_options',
        'sanitize_callback' => 'sanitize_text_field',
    )	);
    
    // Add a control to upload the logo 1
    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'ccn_imago_logo_1',
    array(
    'label' => __('Logo 1'),
    'section' => 'title_tagline',
    'settings' => 'ccn_imago_logo_1',
    ) ) );
    

    // add a setting for the maison imago logo 
    $wp_customize->add_setting('ccn_imago_maison_logo', array(
        'capability'     => 'edit_theme_options',
        'sanitize_callback' => 'sanitize_text_field',
    )	);
    
    // Add a control to upload the maison imago
    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'ccn_imago_maison_logo',
    array(
    'label' => __('Maison imago logo'),
    'section' => 'title_tagline',
    'settings' => 'ccn_imago_maison_logo',
    ) ) );
}
    add_action( 'customize_register', 'ccn_imago_site_identity_feature_customizer' );
?>